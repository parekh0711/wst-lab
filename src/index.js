const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const app = express();
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json())
app.get('/', (req, res) => {
    res.json({
        message: 'WST is cool'
    });
});
const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`App on ${port}`);
});

require("./config/mongoose.js")(app);
app.use('/files', express.static("files"));
require('./routerHandler')(app)
